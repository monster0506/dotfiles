require "config.appearence.colorizer"
require "config.appearence.gitsigns"
require "config.appearence.evil"
require "config.appearence.ufo"
require "config.appearence.statuscol"

-- vim:foldmethod=marker foldlevel=0
