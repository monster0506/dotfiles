require("nvim-autopairs").setup(
    {
        -- {{{
        map_c_w = true,
        disable_in_macro = true

        -- }}}
    }
)

-- vim:foldmethod=marker foldlevel=0
