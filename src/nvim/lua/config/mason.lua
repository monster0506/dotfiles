require("mason").setup(
    {
        -- {{{
        ui = {
            -- {{{
            icons = {
                -- {{{
                package_installed = "✓",
                package_pending = "➜",
                package_uninstalled = "✗"
                -- }}}
            }
            -- }}}
        }
        -- }}}
    }
)

-- vim:foldmethod=marker foldlevel=0
