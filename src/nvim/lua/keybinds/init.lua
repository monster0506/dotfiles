require("keybinds.center")
require("keybinds.commands")
require("keybinds.floaterm")
require("keybinds.fzf")
require("keybinds.git")
require("keybinds.keybindings")
require("keybinds.leap")
require("keybinds.lsp")
require("keybinds.neogen")
require("keybinds.telescope")
require("keybinds.toggle")
require("keybinds.trouble")
require("keybinds.windows")

-- vim:foldmethod=marker foldlevel=0
