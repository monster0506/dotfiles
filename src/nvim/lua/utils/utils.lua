local M = {}

M.hexChars = "0123456789abcdef"

M.kind_icons = {
    Class = "  ",
    Codeium = "  ",
    Color = "  ",
    Constant = "",
    Constructor = "",
    Enum = "",
    EnumMember = "  ",
    Event = "",
    Field = "  ",
    File = "",
    Folder = "  ",
    Function = "",
    Interface = "",
    Keyword = "",
    Method = "",
    Module = "",
    Operator = "  ",
    Property = "  ",
    Reference = "  ",
    Snippet = "",
    Struct = "  ",
    Text = "  ",
    TypeParameter = "  ",
    Unit = "  ",
    Value = "",
    Variable = "  "
}

return M
