require("autocmds.crates")
require("autocmds.format")
require("autocmds.highlights")
require("autocmds.mkdir")

-- vim:foldmethod=marker foldlevel=0
